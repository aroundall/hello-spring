package org.hello.spring.ioc;

import java.util.UUID;

/**
 * Roy Yang
 * 30/07/2017
 */
public class JdbcPersonProvider implements PersonProvider
{
    @Override
    public Person get(Long id) {
        return new Person().setId(id).setName(String.format("JDBC Person %s", UUID.randomUUID().toString().substring(0, 8)));
    }
}
