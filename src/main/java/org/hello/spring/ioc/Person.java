package org.hello.spring.ioc;

/**
 * Roy Yang
 * 30/07/2017
 */
public class Person {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public Person setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }
}
