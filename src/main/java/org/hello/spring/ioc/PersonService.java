package org.hello.spring.ioc;

import java.util.Objects;

/**
 * Roy Yang
 * 30/07/2017
 */
public class PersonService {
    private PersonProvider personProvider;

    public String getPersonName(Long personId) {
        Objects.nonNull(personId);

        return personProvider.get(personId).getName();
    }

    public PersonService setPersonProvider(PersonProvider personProvider) {
        this.personProvider = personProvider;
        return this;
    }
}
