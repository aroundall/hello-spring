package org.hello.spring.ioc;

/**
 * Roy Yang
 * 30/07/2017
 */
public interface PersonProvider {
    Person get(Long id);
}
