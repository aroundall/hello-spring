package org.hello.spring.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Roy Yang
 * 15/08/2017
 */
@Aspect
public class Logging {
    static final Logger logger = LoggerFactory.getLogger(Logging.class);

    @Before("execution(* org.hello.spring.aop.SomeBiz.*(..))")
    public void beforeMethodExe() {
        logger.debug("before exe method");
    }
}
