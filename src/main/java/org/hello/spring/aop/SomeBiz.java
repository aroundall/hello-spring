package org.hello.spring.aop;

/**
 * Roy Yang
 * 15/08/2017
 */
public class SomeBiz {
    public void doSth() {
        //
    }

    public long doSthAndReturn() {
        //
        return 20L;
    }
}
