import org.hamcrest.CoreMatchers;
import org.hello.spring.ioc.PersonService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Roy Yang
 * 30/07/2017
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("/person-service-jdbc.xml")
public class PersonServiceJdbcTest {
    @Autowired
    private PersonService service;

    @Test
    public void findPersonName() {
        String name = service.getPersonName(100L);
        Assert.assertThat(name, CoreMatchers.startsWith("JDBC"));
    }
}
