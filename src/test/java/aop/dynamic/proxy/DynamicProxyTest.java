package aop.dynamic.proxy;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Proxy;
import java.util.Map;

/**
 * Roy Yang
 * 15/08/2017
 */
public class DynamicProxyTest {
    static final  Logger logger = LoggerFactory.getLogger(DynamicProxyTest.class);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void hah() {
        Map map = (Map) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[] {Map.class}, (proxy, method, args) -> {
            logger.debug("I'd like to fake the method ({}) calling", method.getName());
            if (method.getName().equals("get")) {
                return 45;
            } else {
                throw new UnsupportedOperationException("hahh");
            }
        });

        Assert.assertThat(map.get(1), CoreMatchers.equalTo(45));
        Assert.assertThat(map.get(122), CoreMatchers.equalTo(45));

        expectedException.expect(UnsupportedOperationException.class);
        map.put("12", "wrong");
    }
}
