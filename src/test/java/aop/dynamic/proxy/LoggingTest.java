package aop.dynamic.proxy;

import org.hamcrest.CoreMatchers;
import org.hello.spring.aop.SomeBiz;
import org.hello.spring.ioc.PersonService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Roy Yang
 * 30/07/2017
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("/aop/aop-logging-ctx.xml")
public class LoggingTest {
    @Autowired
    private SomeBiz biz;

    @Test
    public void loggingBeforeMethodCall() {
        biz.doSth();
    }
}
